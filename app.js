//require lib
var express = require("express");
var ejs = require("ejs");
var app = express();
var bodyParser = require("body-parser");
var cookieParser = require("cookie-parser");
var mongoose = require("mongoose");
var Schema = mongoose.Schema;
const session = require("express-session");
var async = require("async");
var all_user = new Object();
const nodemailer = require("nodemailer");
var stripe = require("stripe")("sk_test_bQtAGTurRg7k1Yc5IjksiZbQ00Ku547JuR");
// config สำหรับของ gmail
const transporter = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: "fmt1234567891011@gmail.com", // your email
    pass: "fmt987654321" // your email password
  }
});
//connect database
mongoose.connect(
  "mongodb://localhost:27017/FMT-Database",
  { useNewUrlParser: true },
  function(err) {
    if (err) {
      console.log(err);
    }
  }
);

//create model of database
var userSchema = new Schema({
  name: String,
  username: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  img: String,
  admin: Boolean,
  location: String,
  age: Number, //All=1,0-10=20,10-20=30;
  type_age: Number, //age mod range
  gender: Number, //Male=1,Female=2
  type: String,
  about: String,
  status: Number, //1=trainner,2=student
  request: [{ user_id: String, text: String, read: Boolean }],
  message: [{ user_id: String, text: String, read: Boolean }],
  created_at: Date,
  updated_at: Date,
  userHistory: [{ user_id: String }],
  comment: [{ user_id: String, text: String }]
});
var User = mongoose.model("User", userSchema);
module.exports = User;

/*express option*/
app.use(
  session({
    secret: "seproject",
    resave: true,
    saveUninitialized: true,
    cookie: { maxAge: 600000 } //ms
  })
);
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
//set Directory for Docker
app.set("views", "./views");
app.set("view engine", "ejs");
app.use(express.static("./public"));
app.use(cookieParser());

//Page direction
app.get("/payment", function(req, res) {
  //console.log(req.query.id);
  //console.log("payment");
  res.render("payment");
});
app.get("/charge", function(req, res) {
  //console.log("charge");
  //console.log(req.url);
  var email = req.query.email;
  var name = req.query.name;
  var tel = req.query.tel;
  var cookie = req.cookies.cookieName;
  //console.log(email);
  //console.log(name);
  //console.log(tel);
  var stringShow = "<b>From FMT </b>";
  stringShow += "<p>You Payment with " + name + " </p><p>tel:" + tel + "</p>";
  let mailOptions = {
    from: "FMT", // sender
    to: email, // list of receivers
    subject: "Confirmation Course", // Mail subject
    html: stringShow // HTML body
  };
  transporter.sendMail(mailOptions, function(err, info) {
    if (err) console.log(err);
    else console.log(info);
  });
  res.redirect("/");
});
app.post("/query_realtime_noti", function(req, res) {
  //console.log("POST_noti");
  var cookie = req.cookies.cookieName;
  var current_user;
  var allnoti = [];
  async function find_current() {
    //console.log("hello");
    await User.findOne({ _id: cookie }, async function(err, current) {
      current_user = current;
    });
  }
  async function query_noti() {
    //console.log("hello");
    //console.log(current_user.request.length);
    current_user.request.forEach(async function(message) {
      //console.log("hello");
      //console.log(message);
      var id = message.user_id;
      //console.log(id);
      var text = message.text;
      var user = all_user[id];
      var read = message.read;
      //console.log(all_user);
      await allnoti.push({ user_id: id, text, read: true, _id: message._id });
    });
  }
  async function f1() {
    //console.log(allnoti);
    var condition = { request: allnoti };
    User.findOneAndUpdate({ _id: cookie }, condition, function(err) {
      if (err) console.log(err);
    });
  }
  async.waterfall([find_current, query_noti, f1]);
});
app.get("/query_realtime_noti", function(req, res) {
  //console.log("noti");
  //console.log(all_user)
  var cookie = req.cookies.cookieName;
  var current_user;
  async function find_current() {
    await User.findOne({ _id: cookie }, async function(err, current) {
      current_user = current;
    });
  }
  //Query Message
  var allnoti = [];
  //console.log(current_user);
  async function query_noti() {
    //console.log(current_user.request.length);
    current_user.request.forEach(async function(message) {
      //console.log("hello");
      //console.log(message);
      var id = message.user_id;
      var text = message.text;
      var user = all_user[id];
      var read = message.read;
      //console.log(all_user);
      await allnoti.push({ user, text, read });
    });
  }
  //Render Page
  function f1() {
    //console.log(allmessage);
    //console.log(allnoti);
    res.json(allnoti);
  }
  async.waterfall([find_current, query_noti, f1]);
});

app.post("/query_realtime_message", function(req, res) {
  var cookie = req.cookies.cookieName;
  var current_user;
  var allmessage = [];
  async function find_current() {
    await User.findOne({ _id: cookie }, async function(err, current) {
      current_user = current;
    });
  }
  async function query_message() {
    //console.log(current_user.request.length);
    current_user.message.forEach(async function(message) {
      //console.log("hello");
      //console.log(message);
      var id = message.user_id;
      var text = message.text;
      var user = all_user[id];
      var read = message.read;
      //console.log(all_user);
      await allmessage.push({
        user_id: id,
        text,
        read: true,
        _id: message._id
      });
    });
  }
  async function f1() {
    var condition = { message: allmessage };
    User.findOneAndUpdate({ _id: cookie }, condition, function(err) {
      if (err) console.log(err);
    });
  }
  async.waterfall([find_current, query_message, f1]);
});
app.get("/query_realtime_message", function(req, res) {
  //console.log("message");
  //console.log(all_user)
  var cookie = req.cookies.cookieName;
  var current_user;
  //Query Message
  var allmessage = [];
  async function find_current() {
    await User.findOne({ _id: cookie }, async function(err, current) {
      current_user = current;
    });
  }
  async function query_message() {
    //console.log(current_user.message.length);
    current_user.message.forEach(async function(message) {
      //console.log("hello");
      var id = message.user_id;
      var text = message.text;
      var user = all_user[id];
      var read = message.read;
      //console.log(all_user);
      await allmessage.push({ user, text, read });
    });
  }
  //Render Page
  function f1() {
    //console.log(allmessage);
    res.json(allmessage);
  }
  async.waterfall([find_current, query_message, f1]);
});
app.post("/message/:id", function(req, res) {
  var cookie = req.cookies.cookieName;
  var id = req.body.id_message;
  console.log(req.body);
  var text = req.body.text_message;
  console.log(text);
  var condition = {};
  User.findOne({ _id: id }, function(err, user) {
    message = user.message;
    message.push({ user_id: cookie, text: text, read: false });
    //console.log(message);
    condition = { message };
    User.findOneAndUpdate({ _id: id }, condition, function(err) {
      if (err) console.log(err);
    });
  });
  res.redirect("/user/" + id);
});
app.post("/request/:id", function(req, res) {
  var cookie = req.cookies.cookieName;
  var id = req.body.id_request;
  var request;
  var condition = {};
  User.findOne({ _id: id }, function(err, user) {
    //console.log(user);
    request = user.request;
    var valid = false;
    request.forEach(function(r) {
      if (r.user_id == id) valid = true;
    });
    if (!valid) {
      request.push({ user_id: cookie, text: "request", read: false });
      condition = { request };
      User.findOneAndUpdate({ _id: id }, condition, function(err) {
        if (err) console.log(err);
      });
    }
  });
  res.redirect("/user/" + id);
});
app.post("/signUp", function(req, res) {
  var status = req.body.status;
  var Name = req.body.Name;
  var username = req.body.username;
  var psw = req.body.psw;
  var repsw = req.body.repsw;
  var location = req.body.province;
  var age = req.body.age;
  var type_age = -1;
  if (age > 0){
    if(age>=10 && age<=19) type_age=1;
    else if(age>=20 && age<=29) type_age=2;
  }
  var gender = req.body.gender;
  var img = req.body.img;
  var type = req.body.Type;
  console.log(location);
  console.log(type);
  //console.log(req.body.img);
  if (img == "")
    img =
      "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMkAAAD7CAMAAAD3qkCRAAAAkFBMVEWEgoT8/vz///+Bf4F+fH6Fg4V8eXuIhoj6/PqBgIKcmpyMiozy8/J+fX/2+Pb5+fm4triTkZPn6eji4OLf4uHDxsempKaUmZuqq6u4uLjn6ezKysqeoqSWlZnU2Njt7+7Gx83P0tLCwMKlq6zZ29+2ur7M0dGJjpCzsbTZ2tjIx8uusLarqKvQ1NTj5enKzNJ+NtliAAAKVElEQVR4nO2dC3OaQBDHce8AERBfEBWVarSa1qTf/9sVROR4mOjtIiZz/5nMdNJW7if32L3b3dM6P0Va2w0gkyJ5PimS55MieT4pkueTInk+KZLnkyJ5Pj2EBM5q9CFNk8Tt95xRrEn843gN4jRJEjd7tApnh6AXaxj/HA8z/81pCKYxkri9401vaJtMlKvbvSD0moBphiRu6Phoa3HTtYqS3y3G9M9sgAQ6znhjGjUQOY2xXzvEjyUnicfGOtD4Jxgpi9v77ZD2MWoSGPm9uj5Vw2Id3yhRaEkgGgytmzhOLPbGo2MhJYGDftv7yFDYy5wMhY4EvPX+y+FRYXHXXSIWMhJwBto97yOTsSEa+FQkMFq4EhyxrGlEgkJEAqOhJQcSq0cyWGhIwOvJ9Kyz2J6ig9GQeLJd64zCCVBISLyNfNdKUQhmYxKSNxvRt1KU3giLQkHSxwySDGWB7WAEJLC5e0GsQzl0WyfxUKP9IuMX7qXgSeCI71spygSFgiaBuU0DEk9gUbskOyKQZKhgXgqaJCKYuDISe4VAwZJAiFwURfEjwrnHkvQPZK8klvXeGglMhoQgGgv6bZF01jSLyQVlKz1SkCR9qsUkI1m0RRKZpCDx8ig95nEkMDaISbi0zYIkGVAYj6LYS0skQ9phEsuUNVmQJNSdS9P0UPKloEjgg57Eem2FhMTHKooFko3BkbySDxONLedtkNAPeE0bSjpcKBKHyskSZb8/ngQ+miDR/RZI1tS2ykltkPh6AyB81gLJrAGQdkimDUxdMYlcaxTJDyOZEe6rXGRtfsrc1cosvG1iPZHdvkNZK1EDa/zPsSBZ0Ip/sqP3TyzJhRFJQr61Evvxsnt3SD+efho2W9pbWVAPFOlhgiX5R9295I/okLupDvWKore1L9yd0c5efNcWCbzRDhT3oy2STkQ65tlC/ngOfc7oU07E/J98S9AkK0LbCxWH81Qnppb8eMeP+C2li2ItECGR2Hfyh3QWtjbyAUXYE9O/tLOwIT9QsCTEmxJ8/WNI/vyU83j+rzW7i/hUS/4QW5FcRDsLYyII0bYwravV3iwMb7SxRLp8KCQ6vmtPSrJvj2REunnXpn9C7Gm9thdz55Aujbw9C5I44IOH8i3BR9mSxqYicjXRJB9LOhQmG+lBQdIBQveXBYjIejzJmG4bkg8QDSHI2jiQjXnpeDsako5DZbCwJSbvjCK76Y3opfBpq1kbibBJgKnYst1MmkQOhTfP9i1nNyWCOcGiYmHSaKhIYpsFvRVp+Mg2EOViz7HGPWpRPIkqq3yGJEGlaJ1ElekfIacvyVAoQWQ1C3AbxDa+bgEZyQSzycIlY7pEkVXE6GIi042nqVnQSV6K/ESMTV4+ia7eSl/eJrbl9yFyEVaOeZd1VAyE956LsJqPE8iNFPaCrryQiJBENsyezyg6F2mFJcnEGl0yTaMkUpKVDAlqQ0UQbf2uH0MCLxIoz0kis93NepIBwiXRkshUYmALrGeS6gl6FyK7XxQtiUyEFJvSPJyUxJOZuyxUbZJcpOvJhxQJJqhLECnJRoZENn+xLFISqTNH2ZzSsigtSLlzYPyuSipKkq1UcOdQPkS4INK5S8prtGmMlSdYGXlAU6STjqTrxb783SjMmu6wO8Kp6Ei8JChgcueg5z0f+psJxfPp9rvS0trv9xRUZPosduEh2nkEDSDbg5x/pH/Y3PFCzO2pJC+MB0+03wWZ9QT+jS+FabtO9n/W46fZTYXZxXq6bV+CaS9Ckhz08PMX0ZnWeD/Mo4AOX59AsP1OaDusjAPaSaE5Z4wWlp5HX/e/zNE2FhOxlrjXYyZ6H5KCBJLUDRbkL+WLLWLm/il0Jvita2yIrWZLEVngnWpfFSxB/3pNblbOl4FTIUbWQ14qQBC34hxPh0B8KnR18K+tK3zpl++nSBOk+N92T7Ghv1qeu1LBFITVsi6chenHci+C0XlHxsXE32AzmMFbT81seLuzwlI9OuiV18KXv6u3BoQZsomyiuVJAGA865nCNGUWjz37a6s08Pm0ZmvL2WfAuOMHOZLkgePBsHT3QSW8v78Q/wHj27qPEkrlseCheVoA/Wj0L3ANXu48bF/5p+v8CzcPtfceeOKZMQ9G0tfV3EcS96hosj4s3QrFSUZlTYDJOcyILVb1n1gse8AX4Ujy7p17SADm4SCw3av3adTkH0M/TK4Rsfz6flOps8zc5SEcy1zxdDMJgBMel2b9y8iaMaxrwGSv8VWnvmk1ka2M6cNgN78b5kaSeJ4KzK8vN9FrikDAa/IXV15JvY8ZP8d8GcRd9R6YW0jAi8LhpzcYXZpQiWQE7yUZCGxfa+vC7mo0KOPG/jCO+je/mi9JkjE+sG/iSIyn8oLRPXef2j1TGH/u9nN3eFhNboT5ggRgsgnM27exzN/Fp15OtquMnVvy1RiLYfy3W6azz0jiQb4Ohvfd/FOM04rN3IsNULWqILoloYgxyw5mb52vYK6SJMbI0b7vBqNk2SgYHMKXzqp51jC4NWaaWab9+sXcXE8CHWe+YTcOjoJcMXEXxkJTq9Ep3l0hYcywptuof3U+qyGJzZFJ2ONykUGF0A1vL5pdLyVX6v4wPWboh/XkymWCFRLojMKD/ekC+Kn2eScqWSJGaTaYy4S5MNc++rX9rEgSOxzhdCl1m1QmfknrgXHJEjGLz5JNXGHWcOrPKywCSQw6P5Qt9fufs7h8XjkZrfBSrizvtz2DafbLpjQ1ZyTQ6UfJAoiPKjfOnmNNkJQpevpSh5K5GHcXW0dwFFIS8EbrI3dJ8pQyf6s/rcwZwkJ/8d4RYsZ+kI//hASc98FSfoyXPz+tbl7Xe8SFPqTIv2HcOobnKUaLOfwAMVdVpc/TL72a/yBU43OoLhthem932uXQOpthdQ8Ep9Tf6s4q3zp7zec1uhxupi39ePRrgcxK/vkn97rpS6l0LzP3jmVOJK8/kVuDOWrtuCI79begfCgkXD8Rkd+iQF87UMsrn3qll2JcznuaKA/fhLKsGNgWvyg9X0yaKKrehMzztj0UYqFZvoXtNFI6tgFxv3te/sQjISO3yD6+C8nljq+uL4wHfrGSIWyvbXfKnWQ2ST4g2P7inzRTLLoR5RWoZ4L7eyFB3RX6YFnnQ0fBIM5JMAb9w5VV5RGuQBNIwkbuHmhG2XIOm5reRZhR/wCx1N/yBlUSCtfkgWJpSTHnb3Xugrdv1LkudYIjYZV3MzeLuFJWw0pjHmAkWO/ZLEBbAKh5nXz2Qimps4MPk+8ForGpkwwJ8Tdnj/F7da5zqg+sBbue7VOvmNRdfIRYCJ3uL/H7N1KLnrYK2wPEjg4Ui5EaiVcMv75Z59ISgxj6BVfrdMj9bdxFQdzvOIUxcdrTm38/kLjh3aj4G3MEsPuGJPGiPipuSVg7oL914BFif7fF0c2Czpj0Vt2HySpva9mTO0K8n0rlF2BNJXPon0/mtzLolZSUlJSUlJSUlJSUlJSUlJSUlJSUlJSUlJSUlJSUlJSUlJSUlJSUlJTu0n8PKb4QFOjexAAAAABJRU5ErkJggg==";
  var data = {
    name: Name,
    username: username,
    password: psw,
    status: status,
    location: location,
    age: age,
    type_age: type_age,
    gender: gender,
    img: img,
    type: type
  };
  var user = new User(data);
  user.save();
  res.redirect("/");
});
app.post("/config", function(req, res) {
  //console.log(req.body);
  var img = req.body.img;
  //console.log(img);
  var name = req.body.name;
  //console.log(name);
  var location = req.body.location;
  var age = req.body.age;
  var type_age = -1;
  if (age > 0) type_age = age / 10;
  var type = req.body.age;
  var gender = req.body.gender;
  //check password
  var old_password = req.body.old_password;
  var new_password = req.body.new_password;
  var confirm_password = req.body.confirm_password;
  //console.log(new_password);
  var about = req.body.about;
  var condition;
  var check_mark = true;
  var cookie = req.cookies.cookieName;
  //Query Notification
  var cookie = req.cookies.cookieName;
  var allnoti = [];
  async function query() {
    await User.findOne({ _id: cookie }, async function(err, current_user) {
      await current_user.request.forEach(async function(id) {
        await User.findOne({ _id: id }, async function(err, user) {
          //console.log("hello");
          await allnoti.push(user);
        });
      });
    });
  }
  //Query Message
  var allmessage = [];
  async function query2() {
    await User.findOne({ _id: cookie }, async function(err, current_user) {
      await current_user.message.forEach(async function(message) {
        var id = message.user_id;
        var text = message.text;
        //console.log(id);
        await User.findOne({ _id: id }, async function(err, user) {
          //console.log("hello");
          if (user) await allmessage.push({ user, text });
        });
      });
    });
  }
  //Render Page
  function f1() {
    if (old_password == "") {
      //don't change password
      var condition = {
        img: img,
        name: name,
        location: location,
        age: age,
        type_age: type_age,
        type: type,
        gender: gender,
        about: about
      };
    } else {
      User.findOne({ _id: cookie }, function(err, current_user) {
        var current_password = current_user.password;
        if (old_password != current_password) {
          res.render("edit_profile", {
            users: [],
            passwordFail: 1,
            passwordNoMatch: false,
            current_user: current_user,
            allmessage: allmessage,
            allnoti: allnoti
          });
          check_mark = false;
        } else if (new_password != confirm_password) {
          res.render("edit_profile", {
            users: [],
            passwordFail: 0,
            passwordNoMatch: true,
            current_user: current_user,
            allmessage: allmessage,
            allnoti: allnoti
          });
          check_mark = false;
        } else {
          var condition = {
            password: new_password,
            img: img,
            name: name,
            location: location,
            age: age,
            type_age: type_age,
            type: type,
            gender: gender,
            about: about
          };
        }
      });
    }
    if (check_mark) {
      User.findOneAndUpdate({ _id: cookie }, condition, function(err) {
        if (err) console.log(err);
      });
      res.redirect("/");
    }
  }
  async.waterfall([query, query2, f1]);
});
app.post("/", function(req, res) {
  var name = req.body.Name;
  var gender = req.body.Gender;
  var type_age = req.body.Age;
  var location = req.body.Province;
  var type = req.body.Type;
  //console.log(name + " " + gender + " " + type_age+" "+location+" "+type);
  //Query Notification
  var cookie = req.cookies.cookieName;
  var allnoti = [];
  async function query() {
    await User.findOne({ _id: cookie }, async function(err, current_user) {
      await current_user.request.forEach(async function(id) {
        await User.findOne({ _id: id }, async function(err, user) {
          //console.log("hello");
          await allnoti.push(user);
        });
      });
    });
  }
  //Query Message
  var allmessage = [];
  async function query2() {
    await User.findOne({ _id: cookie }, async function(err, current_user) {
      await current_user.message.forEach(async function(message) {
        var id = message.user_id;
        var text = message.text;
        //console.log(id);
        await User.findOne({ _id: id }, async function(err, user) {
          //console.log("hello");
          if (user) await allmessage.push({ user, text });
        });
      });
    });
  }
  //Render Page
  function f1() {
    if (name == "") {
      if (type_age == "0" && gender == "0") {
        console.log("FINDD");
        User.find({location:location,type:type}, function(err, users) {
          if (err) {
            console.log(err);
          } else {
            User.findOne({ _id: cookie }, function(err, current_user) {
              res.render("index", {
                users: users,
                current_user: current_user,
                allnoti: allnoti,
                allmessage: allmessage
              });
            });
          }
        });
      } else if (type_age == "0") {
        User.find({ gender:gender,location:location,type:type}, function(err, users) {
          if (err) {
            console.log(err);
          } else {
            User.findOne({ _id: cookie }, function(err, current_user) {
              res.render("index", {
                users: users,
                current_user: current_user,
                allnoti: allnoti,
                allmessage: allmessage
              });
            });
          }
        });
      } else if (gender == "0") {
        User.find({ type_age:type_age,location:location,type:type}, function(err, users) {
          if (err) {
            console.log(err);
          } else {
            User.findOne({ _id: cookie }, function(err, current_user) {
              res.render("index", {
                users: users,
                current_user: current_user,
                allnoti: allnoti,
                allmessage: allmessage
              });
            });
          }
        });
      } else {
        User.find({ gender:gender, type_age:type_age ,location:location,type:type}, function(err, users) {
          if (err) {
            console.log(err);
          } else {
            User.findOne({ _id: cookie }, function(err, current_user) {
              res.render("index", {
                users: users,
                current_user: current_user,
                allnoti: allnoti,
                allmessage: allmessage
              });
            });
          }
        });
      }
    } else {
      if (type_age == "0" && gender == "0") {
        User.find({ name: name ,location:location,type:type}, function(err, users) {
          console.log("INNN");
          if (err) {
            console.log(err);
          } else {
            User.findOne({ _id: cookie }, function(err, current_user) {
              res.render("index", {
                users: users,
                current_user: current_user,
                allnoti: allnoti,
                allmessage: allmessage
              });
            });
          }
        });
      } else if (type_age == 0) {
        User.find({ name: name, gender:gender ,location:location,type:type}, function(err, users) {
          if (err) {
            console.log(err);
          } else {
            User.findOne({ _id: cookie }, function(err, current_user) {
              res.render("index", {
                users: users,
                current_user: current_user,
                allnoti: allnoti,
                allmessage: allmessage
              });
            });
          }
        });
      } else if (gender == 0) {
        User.find({ name: name, type_age:type_age ,location:location,type:type}, function(err, users) {
          if (err) {
            console.log(err);
          } else {
            User.findOne({ _id: cookie }, function(err, current_user) {
              res.render("index", {
                users: users,
                current_user: current_user,
                allnoti: allnoti,
                allmessage: allmessage
              });
            });
          }
        });
      } else {
        User.find({ name: name, gender, type_age:type_age ,location:location,type:type}, function(err, users) {
          if (err) {
            console.log(err);
          } else {
            User.findOne({ _id: cookie }, function(err, current_user) {
              res.render("index", {
                users: users,
                current_user: current_user,
                allnoti: allnoti,
                allmessage: allmessage
              });
            });
          }
        });
      }
    }
  }
  async.waterfall([query, query2, f1]);
});

app.get("/config/:_id", function(req, res) {
  var cookie = req.cookies.cookieName;
  //Query Notification
  var cookie = req.cookies.cookieName;
  var allnoti = [];
  async function query() {
    await User.findOne({ _id: cookie }, async function(err, current_user) {
      await current_user.request.forEach(async function(id) {
        await User.findOne({ _id: id }, async function(err, user) {
          //console.log("hello");
          await allnoti.push(user);
        });
      });
    });
  }
  //Query Message
  var allmessage = [];
  async function query2() {
    await User.findOne({ _id: cookie }, async function(err, current_user) {
      await current_user.message.forEach(async function(message) {
        var id = message.user_id;
        var text = message.text;
        //console.log(id);
        await User.findOne({ _id: id }, async function(err, user) {
          //console.log("hello");
          if (user) await allmessage.push({ user, text });
        });
      });
    });
  }
  //Render Page
  function f1() {
    User.findOne({ _id: cookie }, function(err, current_user) {
      res.render("edit_profile", {
        passwordFail: false,
        users: [],
        passwordNoMatch: false,
        current_user: current_user,
        allmessage: allmessage,
        allnoti: allnoti
      });
    });
  }
  async.waterfall([query, query2, f1]);
});
app.get("/addcomment", function(req, res) {
  var trainee_id = req.cookies.cookieName;
  var trainer_id = req.query.id;
  var text = req.query.text;
  if(text.length > 0){
    User.update(
      {
        _id: trainer_id
      },
      {
        $push: { comment: { user_id: trainee_id, text } }
      },
      o => res.redirect("/")
    );
}else{
  User.update(
      o => res.redirect("/")
    );
}

});
app.get("/editPaymentNoti", function(req, res) {
  //console.log("editPaymentNoti")
  var isAccept = req.query.isAccept;
  var trainee_id = req.query.id;
  var trainer_id = req.cookies.cookieName;
  //console.log("trainner:",trainer_id);
  //console.log("trainnee:",trainee_id);
  if (isAccept == "true") {
    //console.log("Editttt");
    //console.log(trainee_id);
    //console.log(trainer_id);
    User.update(
      {
        _id: trainee_id
      },
      {
        $push: {
          request: { user_id: trainer_id, text: "pay", read: false }
        }
      },
      { multi: true },
      o => {
        res.send("Add Payment");
      }
    );
  }
});
app.get("/editHistory", function(req, res) {
  //console.log(req.query);
  var isAccept = req.query.isAccept;
  var trainee_id = req.query.id;
  var trainer_id = req.cookies.cookieName;
  var option = req.query.option;
  if (option == 1) {
    console.log("OPTION!!");
    var temp = trainee_id;
    trainee_id = trainer_id;
    trainer_id = temp;
  }
  console.log("trainner:", trainer_id);
  console.log("trainnee:", trainee_id);
  console.log(option);
  if (isAccept == "true") {
    console.log("TRUEEE");
    User.update(
      {
        _id: trainer_id
      },
      {
        $pull: {
          request: { user_id: trainee_id }
        }
      },
      { multi: true },
      o => {
        //console.log("hello");
        User.update(
          {
            _id: trainee_id
          },
          {
            $pull: { request: { user_id: trainer_id } },
          },
          o => {
            User.update(
              {
                _id: trainee_id
              },
              {
                $push: {request: {user_id: trainer_id,text: "complete",read: false},},
                $push: { userHistory: { user_id: trainer_id },}
              }, o=>{
                res.send("Accept")
              }
            );
          }
        );
      }
    );
  } else {
    console.log("REMOVE REQUEST");
    User.update(
      {
        _id: trainer_id
      },
      {
        $pull: {
          request: { user_id: trainee_id }
        }
      },
      { multi: true },
      o => res.send("Decline")
    );
  }
});
app.get("/history/", function(req, res) {
  var cookie = req.cookies.cookieName;
 
  //Query History
  var user_history_buf = [];
  async function query3() {
    await User.findOne({ _id: cookie }).lean().exec(async function(err,current_user) {
      console.log("FIND")
      console.log(current_user.userHistory);
      //console.log(current_user.userHistory);
      await current_user.userHistory.forEach(async function(history) {
        var id = history.user_id;
        await User.findOne({ _id: id }, async function(err, user) {
          if (user) await user_history_buf.push(user);
        });
      });
    console.log(user_history_buf);
    if (!req.session.login) {
      res.render("signIn", { users: [], current_user: [] });
    } else {
      User.find({}, function(err, users) {
        User.findOne({ _id: cookie }, function(err, current_user) {
          res.render("history", {
            user_history: user_history_buf,
            users: users,
            current_user: current_user,
          });
        });
      });
    }
    });
  }
  async.waterfall([query3]);
});

app.get("/config", function(req, res) {
  var cookie = req.cookies.cookieName;
  res.redirect("config/" + cookie);
});
app.get("/history", function(req, res) {
  var cookie = req.cookies.cookieName;
  res.redirect("history/" + cookie);
});
app.get("/signUp", function(req, res) {
  var cookie = req.cookies.cookieName;
  User.findOne({ _id: cookie }, function(err, current_user) {
    res.render("signUp", { users: [], current_user: current_user });
  });
});

app.get("/signIn", function(req, res) {
  var cookie = req.cookies.cookieName;
  User.find({ _id: cookie }, function(err, current_user) {
    res.render("signIn", { users: [], current_user: current_user });
  });
});
app.post("/signIn", function(req, res) {
  uname = req.body.uname;
  psw = req.body.psw;
  //console.log({username:uname,password:psw})
  User.find({ username: uname, password: psw }, function(err, users) {
    ans = false;
    if (err) {
      console.log(err);
    } else if (users.length > 0) {
      //console.log(users);
      current_user = users[0];
      ans = users.length == 1;
      req.session.login = true;
      var cookie = req.cookies.cookieName;
      res.cookie("cookieName", users[0]._id, {
        maxAge: 900000,
        httpOnly: true
      });
    } else {
      ans = 0;
    }
    res.json(ans);
  });
});
app.get("/", function(req, res) {
  User.find({}, function(err, user) {
    //console.log(user);
    user.forEach(function(u) {
      all_user[u._id] = u;
    });
  });
  //Query Notification
  var cookie = req.cookies.cookieName;
  var allnoti = [];
  async function query() {
    await User.findOne({ _id: cookie }, async function(err, current_user) {
      await current_user.request.forEach(async function(id) {
        await User.findOne({ _id: id }, async function(err, user) {
          //console.log("hello");
          await allnoti.push(user);
        });
      });
    });
  }
  //Query Message
  var allmessage = [];
  async function query2() {
    await User.findOne({ _id: cookie }, async function(err, current_user) {
      await current_user.message.forEach(async function(message) {
        var id = message.user_id;
        var text = message.text;
        //console.log(id);
        await User.findOne({ _id: id }, async function(err, user) {
          //console.log("hello");
          if (user) await allmessage.push({ user, text });
        });
      });
    });
  }
  //Render Page
  function f1() {
    if (!req.session.login) {
      res.render("signIn", { users: [], current_user: [] });
    } else {
      User.find({}, function(err, users) {
        User.findOne({ _id: cookie }, function(err, current_user) {
          res.render("index", {
            users: users,
            current_user: current_user,
            allnoti: allnoti,
            allmessage: allmessage
          });
        });
      });
    }
  }
  async.waterfall([query, query2, f1]);
});
app.get("/user/:id", function(req, res) {
  //console.log("GET SUCCESS!!");
  var cookie = req.cookies.cookieName;
  var id = req.params.id;
  //Query Notification
  var cookie = req.cookies.cookieName;
  var allnoti = [];
  async function query() {
    await User.findOne({ _id: cookie }, async function(err, current_user) {
      await current_user.request.forEach(async function(id) {
        await User.findOne({ _id: id }, async function(err, user) {
          //console.log("hello");
          await allnoti.push(user);
        });
      });
    });
  }
  //Query Message
  var allmessage = [];
  async function query2() {
    await User.findOne({ _id: cookie }, async function(err, current_user) {
      await current_user.message.forEach(async function(message) {
        var id = message.user_id;
        var text = message.text;
        //console.log(id);
        await User.findOne({ _id: id }, async function(err, user) {
          //console.log("hello");
          if (user) await allmessage.push({ user, text });
        });
      });
    });
  }
  //Render Page
  function f1() {
    //TODO: find name in DB
    User.find({ _id: id }, function(err, users) {
      User.findOne({ _id: cookie }, function(err, current_user) {
        res.render("user", { users, current_user, allmessage, allnoti });
      });
    });
  }
  async.waterfall([query, query2, f1]);
});

app.post("/", function(req, res) {
  alert("ERROR");
});
app.get("/logout", function(req, res) {
  req.session.login = false;
  res.redirect("/");
});

app.set("port", 80);
app.listen(app.get("port"), "0.0.0.0", function() {
  console.log("listening on port 80");
});
