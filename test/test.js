const request = require('supertest');
const app = require('../app');
console.log(app);

describe('App', function() {
  it('has the default page', function(done) {
    request(app)
      .get('/')
      .expect(/Welcome to Express/, done);
      done();
  });
}); 
